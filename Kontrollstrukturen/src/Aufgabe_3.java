
import java.util.Scanner;

public class Aufgabe_3 {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		int anzahlMaeuse;
		double einzelpreis, rechnungsbetrag;
		final double MWST = 0.19;
		final double LIEFERPAUSCHALE = 10.0;
		System.out.println("+++ Grosshaendler +++");
		System.out.print("Anzahl der bestellten Mäuse: ");
		anzahlMaeuse = myScanner.nextInt();
		System.out.print("Einzelpreis der Mäuse in Euro: ");
		einzelpreis = myScanner.nextDouble();
		rechnungsbetrag = (anzahlMaeuse * einzelpreis) * (1.0 + MWST);
		
		if (anzahlMaeuse < 10) {
			rechnungsbetrag = rechnungsbetrag + LIEFERPAUSCHALE;
		}
		
		System.out.println("Rechnungsbetrag: " + rechnungsbetrag + " Euro");
		myScanner.close();
	}
}
