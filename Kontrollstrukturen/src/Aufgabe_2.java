import java.util.*;

public class Aufgabe_2 {

	public static void main(String[] args) {

		double nettowert, klSteuersatz, grSteuersatz;
		String tmp;
		Scanner scan = new Scanner(System.in);

		System.out.print("Bitte Nettowert eingeben: ");
		nettowert = scan.nextDouble();
		System.out.print("Erm��igter Stuersatz ([j] - ja, [n] - nein): ");
		tmp = scan.next();

		klSteuersatz = 0.18;
		grSteuersatz = 0.07;

		// Ausgabe
		System.out.print("Das Ergebnis lautet: ");
		if (tmp.equals("j")) {
			System.out.println(nettowert - (nettowert * klSteuersatz));
		} else {
			System.out.println(nettowert - (nettowert * grSteuersatz));
		}

		scan.close();

	}

}