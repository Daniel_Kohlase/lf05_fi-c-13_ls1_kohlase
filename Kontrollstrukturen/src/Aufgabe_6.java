import java.util.Scanner;

public class Aufgabe_6 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		int x;
		double y;
		final double E = 2.781;

		System.out.println("Bitte geben Sie eine ganze Zahl fuer x ein: ");
		x = scan.nextInt();

		if (x <= 0) {
			y = Math.pow(E, x);
			System.out.println("Exponentielle Funktion");
		}

		else if (x > 0 && x <= 3) {
			y = Math.pow(x, 2) + 1;
			System.out.println("Quadratische Funktion");
		} 
		
		else {
			y = 2 * x + 4;
			System.out.println("Lineare Funktion");
		}

		System.out.println("x = " + x);
		System.out.println("y = " + y);

	}
}
