import java.util.Scanner;

class Fahrkartenautomat_Methoden {

	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double r�ckgabebetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag) - zuZahlenderBetrag;
		rueckgeldAusgeben(r�ckgabebetrag);
		fahrkartenAusgeben();
		tastatur.close();
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		byte ticketAnzahl;

		System.out.print("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = tastatur.nextDouble();

		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextByte();
		
		return zuZahlenderBetrag * ticketAnzahl;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double nochZuZahlen = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("\nNoch zu zahlen: %.2f Euro\n", nochZuZahlen);
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {

		// R�ckgeldberechnung und -Ausgabe
		// -------------------------------
		if (r�ckgabebetrag > 0.0) {
			System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}
	}

	public static void fahrkartenAusgeben() {

		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");

	}

}