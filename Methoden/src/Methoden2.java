
import java.util.Scanner;

class Methoden2 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int volumenArt = 0;
		double a, b, c;

		System.out.println("Welche Art von Volumen m�chten Sie berechnen? Tippen Sie eine der folgenden Ziffern:");
		System.out.println("'1' f�r W�rfelvolumen.");
		System.out.println("'2' f�r Quadervolumen.");
		System.out.println("'3' f�r Pyramidenvolumen (quadratisch).");
		System.out.println("'4' f�r Kugelvolumen.");
		System.out.print("Ihre Auswahl: ");

		volumenArt = scan.nextInt();

		switch (volumenArt) {
		case 1:
			System.out.print("L�nge Seite a: ");
			a = scan.nextDouble();
			System.out.println("Volumen: " + volumenWuerfel(a) + " VE");
			break;
		case 2:
			System.out.print("L�nge Seite a: ");
			a = scan.nextDouble();
			System.out.print("L�nge Seite b: ");
			b = scan.nextDouble();
			System.out.print("L�nge Seite c: ");
			c = scan.nextDouble();
			System.out.println("Volumen: " + volumenQuader(a, b, c) + " VE");
			break;
		case 3:
			System.out.print("L�nge Seite a: ");
			a = scan.nextDouble();
			System.out.print("H�he h: ");
			b = scan.nextDouble();
			System.out.println("Volumen: " + volumenPyramide(a, b) + " VE");
			break;
		case 4:
			System.out.print("Radius r: ");
			a = scan.nextDouble();
			System.out.println("Volumen: " + volumenKugel(a) + " VE");
			break;
		}

		scan.close();
	}

	static double volumenWuerfel(double a) {
		double v = a * a * a;
		return v;
	}

	static double volumenQuader(double a, double b, double c) {
		double v = a * b * c;
		return v;
	}

	static double volumenPyramide(double a, double h) {
		double v = a * a * (h / 3);
		return v;
	}

	static double volumenKugel(double r) {
		double v = (4 / 3) * r * r * r * Math.PI;
		return v;
	}

}